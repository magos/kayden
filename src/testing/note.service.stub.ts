import { of, Observable } from 'rxjs';
import { getNotes } from './utilities';
import { Note } from '../app/notes/shared/note';

export class NoteServiceStub {
  getNotes(): Observable<Note[]> {
    return of(getNotes());
  }

  insertNote(note: Note): Observable<Note> {
    return of(note);
  }

  removeNote(id: number): Observable<Note> {
    return of({ id } as Note);
  }
}
