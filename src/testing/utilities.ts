import { Note } from '../app/notes/shared/note';
import { NoteStatus } from '../app/notes/shared/note-status.enum';

export function getNote(): Note {
  return {
    id: 2,
    content: 'example note content',
    title: 'example note title',
    status: NoteStatus.Uncompleted,
    isDisabled: false,
    isSelected: true
  };
}

export function getNotes(): Note[] {
  return [
    {
      id: 1,
      title: 'delectus aut autem',
      content: 'Lorem ipsum',
      status: NoteStatus.New,
      isSelected: false,
      isDisabled: false
    },
    {
      id: 2,
      title: 'quis ut nam facilis et officia qui',
      content: 'Lorem ipsum',
      status: NoteStatus.Completed,
      isSelected: true,
      isDisabled: false
    },
    {
      id: 3,
      title: 'fugiat veniam minus',
      content: 'Lorem ipsum',
      status: NoteStatus.Uncompleted,
      isSelected: false,
      isDisabled: false
    }
  ];
}

export function getNotesForSorting(): Note[] {
  return [
    {
      id: 2,
      title: 'Delectus aut autem 2',
      content: 'Lorem ipsum',
      status: NoteStatus.New,
      isSelected: false,
      isDisabled: false
    },
    {
      id: 1,
      title: 'quis ut nam facilis et officia qui',
      content: 'Lorem ipsum',
      status: NoteStatus.Completed,
      isSelected: true,
      isDisabled: false
    },
    {
      id: 3,
      title: 'fugiat veniam minus',
      content: 'Lorem ipsum',
      status: NoteStatus.Uncompleted,
      isSelected: false,
      isDisabled: false
    },
    {
      id: 5,
      title: 'adelectus aut autem',
      content: 'Lorem ipsum',
      status: NoteStatus.New,
      isSelected: false,
      isDisabled: false
    },
    {
      id: 4,
      title: 'belectus aut autem',
      content: 'Lorem ipsum',
      status: NoteStatus.New,
      isSelected: false,
      isDisabled: false
    },
  ];
}
