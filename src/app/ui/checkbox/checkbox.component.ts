import { Component, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'kd-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent {

  @Output() valueChange: EventEmitter<boolean>;
  @Input() disabled: boolean;
  @Input() selected: boolean;

  constructor() {
    this.selected = false;
    this.disabled = false;
    this.valueChange = new EventEmitter<boolean>();
  }

  checkboxChanged(isChecked: boolean) {
    this.valueChange.emit(isChecked);
  }
}
