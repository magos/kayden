import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { CheckboxComponent } from './checkbox.component';

describe('CheckboxComponent', () => {
  let component: CheckboxComponent;
  let fixture: ComponentFixture<CheckboxComponent>;
  let de: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckboxComponent ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    component.disabled = false;
    component.selected = false;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call checkboxChanged on checkbox click', () => {
    const checkboxEl: HTMLElement = de.query(By.css('input[type="checkbox"]')).nativeElement;
    const checkboxChangedSpy = spyOn(component, 'checkboxChanged');
    checkboxEl.click();
    expect(checkboxChangedSpy).toHaveBeenCalledTimes(1);
  });

  it('should initialize selected property with false by default', () => {
    expect(component.selected).toBe(false);
  });

  it('should initialize disabled property with false by default', () => {
    expect(component.disabled).toBe(false);
  });

  describe('checkboxChanged', () => {
    it('should emit valueChange event', () => {
      component.valueChange.subscribe((isChecked: boolean) => {
        expect(isChecked).toBe(true);
      });
      component.checkboxChanged(true);
    });
  });
});
