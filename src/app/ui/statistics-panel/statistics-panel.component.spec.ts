import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { StatisticsPanelComponent } from './statistics-panel.component';

describe('StatisticsPanelComponent', () => {
  let component: StatisticsPanelComponent;
  let fixture: ComponentFixture<StatisticsPanelComponent>;
  let de: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ StatisticsPanelComponent ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatisticsPanelComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    component.statistics = {
      completed: 4,
      uncompleted: 6,
      total: 10
    }
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render total', () => {
    const totalLabelEl: HTMLElement = de.query(By.css('#kd-total-label')).nativeElement;
    expect(totalLabelEl.innerText).toBe('10');
  });

  it('should render completed', () => {
    const totalLabelEl: HTMLElement = de.query(By.css('#kd-completed-label')).nativeElement;
    expect(totalLabelEl.innerText).toBe('4');
  });

  it('should render uncompleted', () => {
    const totalLabelEl: HTMLElement = de.query(By.css('#kd-uncompleted-label')).nativeElement;
    expect(totalLabelEl.innerText).toBe('6');
  });
});
