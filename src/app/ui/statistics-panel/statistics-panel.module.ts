import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatisticsPanelComponent } from './statistics-panel.component';

@NgModule({
  declarations: [StatisticsPanelComponent],
  imports: [
    CommonModule
  ],
  exports: [StatisticsPanelComponent]
})
export class StatisticsPanelModule { }
