import { Component, Input } from '@angular/core';
import { Statistics } from '../../notes/shared/statistics';

@Component({
  selector: 'kd-statistics-panel',
  templateUrl: './statistics-panel.component.html',
  styleUrls: ['./statistics-panel.component.scss']
})
export class StatisticsPanelComponent {
  @Input() statistics: Statistics;
}
