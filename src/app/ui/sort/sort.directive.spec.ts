import { Component, DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { Subscription } from 'rxjs';

import { SortDirective } from './sort.directive';
import { Note } from '../../notes/shared/note';
import { SortingStateService } from './sorting-state.service';
import { SortDirection } from './sort-direction';
import { getNotesForSorting } from '../../../testing/utilities';

@Component({
  template: `<table>
              <tr>
                <th kdSort="{{columnName}}" [kdSortSource]="notes"></th>
              </tr>
            </table>`
})
class WrapperComponent {
  columnName: string;
  notes: Note[];

  constructor(private sortingStateService: SortingStateService) { }
}

describe('SortDirective', () => {
  let fixture: ComponentFixture<WrapperComponent>;
  let wrapper: WrapperComponent;
  let directive: SortDirective;
  let de: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ SortDirective, WrapperComponent ],
      providers: [ SortingStateService ]
    });

    fixture = TestBed.createComponent(WrapperComponent);
    wrapper = fixture.componentInstance;
    de = fixture.debugElement.query(By.directive(SortDirective));
    directive = de.injector.get(SortDirective);
  });

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('should initialize subscription on initialization', () => {
    expect(directive.subscription instanceof Subscription).toBe(true);
  });

  it('should set default sort direction on ascending', () => {
    expect(directive.direction).toBe(SortDirection.Ascending);
  });

  describe('onDestroy', () => {
    it('should unsubscribe subscriptions', () => {
      const unsubscribeSpy = spyOn(directive.subscription, 'unsubscribe');
      directive.ngOnDestroy();
      expect(unsubscribeSpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('onClick title', () => {
    beforeEach(() => {
      wrapper.notes = getNotesForSorting();
    });

    describe('text column', () => {
      beforeEach(() => {
        wrapper.columnName = 'title';
        fixture.detectChanges();
        de.triggerEventHandler('click', {});
      });

      it('should sort title column in ascending order', () => {
        const firstNoteTitle = wrapper.notes[0].title;
        const secondNoteTitle = wrapper.notes[1].title;
        const thridNoteTitle = wrapper.notes[2].title;
        expect(firstNoteTitle.localeCompare(secondNoteTitle)).toBe(-1);
        expect(secondNoteTitle.localeCompare(thridNoteTitle)).toBe(-1);
        expect(secondNoteTitle.localeCompare(firstNoteTitle)).toBe(1);
        expect(thridNoteTitle.localeCompare(secondNoteTitle)).toBe(1);
      });

      it('should sort title column in descending order after second click', () => {
        de.triggerEventHandler('click', {});
        const firstNoteTitle: string = wrapper.notes[0].title;
        const secondNoteTitle: string = wrapper.notes[1].title;
        const thridNoteTitle: string = wrapper.notes[2].title;
        expect(firstNoteTitle.localeCompare(secondNoteTitle)).toBe(1);
        expect(secondNoteTitle.localeCompare(thridNoteTitle)).toBe(1);
        expect(secondNoteTitle.localeCompare(firstNoteTitle)).toBe(-1);
        expect(thridNoteTitle.localeCompare(secondNoteTitle)).toBe(-1);
      });
    });

    describe('numeric column', () => {
      beforeEach(() => {
        wrapper.columnName = 'id';
        fixture.detectChanges();
        de.triggerEventHandler('click', {});
      });

      it('should sort in ascending order', () => {
        const firstNoteId: number = wrapper.notes[0].id;
        const secondNoteId: number = wrapper.notes[1].id;
        const thridNoteId: number = wrapper.notes[2].id;
        expect(firstNoteId - secondNoteId).toBeLessThan(0);
        expect(secondNoteId - thridNoteId).toBeLessThan(0);
      });

      it('should sort in descending order after second click', () => {
        de.triggerEventHandler('click', {});
        const firstNoteId: number = wrapper.notes[0].id;
        const secondNoteId: number = wrapper.notes[1].id;
        const thridNoteId: number = wrapper.notes[2].id;
        expect(firstNoteId - secondNoteId).toBeGreaterThan(0);
        expect(secondNoteId - thridNoteId).toBeGreaterThan(0);
      });
    });

    it('should switch direction property', () => {
      fixture.detectChanges();
      de.triggerEventHandler('click', {});
      expect(directive.direction).toBe(SortDirection.Descending);
      de.triggerEventHandler('click', {});
      expect(directive.direction).toBe(SortDirection.Ascending);
    });
  });
});
