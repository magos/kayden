import { TestBed } from '@angular/core/testing';

import { Subject } from 'rxjs';

import { SortingStateService } from './sorting-state.service';

describe('SortingStateService', () => {
  let service: SortingStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ SortingStateService ]
    });
    service = TestBed.get(SortingStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should initialize sort property with new Subject', () => {
    expect(service.sort instanceof Subject).toBe(true);
  });

  describe('onDestroy', () => {
    it('should unsubscribe sort subject', () => {
      const unsubscribeSpy = spyOn(service.sort, 'unsubscribe');
      service.ngOnDestroy();
      expect(unsubscribeSpy).toHaveBeenCalledTimes(1);
    });
  });
});
