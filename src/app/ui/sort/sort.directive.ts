import { Directive, HostListener, Input, ElementRef, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { SortDirection } from './sort-direction';
import { SortingStateService } from './sorting-state.service';
import { Subscription } from 'rxjs';

@Directive({
  selector: 'th[kdSort]'
})
export class SortDirective implements OnInit, OnDestroy {

  // tslint:disable:no-input-rename
  @Input('kdSort') columnName: string;
  @Input('kdSortSource') source: any[];
  direction: SortDirection;
  subscription: Subscription;

  constructor(private sortingStateService: SortingStateService,
    private el: ElementRef,
    private renderer: Renderer2) {
      this.subscription = new Subscription();
      this.direction = SortDirection.Ascending;
    }

  ngOnInit(): void {
    this.subscription.add(this.sortingStateService.sort.subscribe((columnName: string) => {
      if (columnName !== this.columnName) {
        this.removeSortingIndicator();
      }
    }));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  @HostListener('click') onClick(): void {
    const comparer = this.getComparer(this.source, this.columnName, this.direction);
    this.source.sort(comparer);
    this.switchSortingIndicator();
    this.direction = this.toggleSortDirection(this.direction);
    this.sortingStateService.sort.next(this.columnName);
  }

  private switchSortingIndicator(): void {
    const sortingIndicatorEl: HTMLElement = this.el.nativeElement.querySelector('.kd-sort-direction-arrow');

    if (sortingIndicatorEl) {
      const indicatorClassName: string = sortingIndicatorEl.classList.contains('fa-arrow-up') ? 'fa-arrow-down' : 'fa-arrow-up';
      this.removeSortingIndicator();
      this.renderer.addClass(sortingIndicatorEl, indicatorClassName);
    }
  }

  private removeSortingIndicator(): void {
    const sortingIndicatorEl: HTMLElement = this.el.nativeElement.querySelector('.kd-sort-direction-arrow');

    if (sortingIndicatorEl) {
      this.renderer.removeClass(sortingIndicatorEl, 'fa-arrow-up');
      this.renderer.removeClass(sortingIndicatorEl, 'fa-arrow-down');
    }
  }

  private toggleSortDirection(currentDirection: SortDirection): SortDirection {
    return currentDirection === SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending;
  }

  private getComparer(source: any[], columnName: string, direction: SortDirection): (a, b) => number {
    return source && typeof source[0][columnName] === 'string' ?
      this.getStringComparer(columnName, direction) :
      this.getDefaultComparer(columnName, direction);
  }

  private getStringComparer(columnName: string, direction: SortDirection): (a, b) => number {
    return (a, b) => {
      return direction === SortDirection.Ascending ?
          a[columnName].localeCompare(b[columnName]) :
          a[columnName].localeCompare(b[columnName]) * -1;
    };
  }

  private getDefaultComparer(columnName: string, direction: SortDirection): (a, b) => number {
    return (a, b) => {
      const comparisonResult = a[columnName] - b[columnName];
      return direction === SortDirection.Ascending ? comparisonResult : comparisonResult * -1;
    };
  }
}
