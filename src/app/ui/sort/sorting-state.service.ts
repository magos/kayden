import { Injectable, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class SortingStateService implements OnDestroy {

  sort: Subject<string>;

  constructor() {
    this.sort = new Subject<string>();
  }

  ngOnDestroy() {
    this.sort.unsubscribe();
  }
}
