import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SortDirective } from './sort.directive';

@NgModule({
  declarations: [SortDirective],
  imports: [
    CommonModule
  ],
  exports: [SortDirective]
})
export class SortModule { }
