import { SortDirection } from './sort-direction';

export interface SortingState {
  column: string,
  direction: SortDirection
}
