import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ModalComponent } from './modal.component';

describe('ModalComponent', () => {
  let component: ModalComponent;
  let fixture: ComponentFixture<ModalComponent>;
  let de: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NgbModule
      ],
      declarations: [ ModalComponent ],
      providers: [ NgbActiveModal ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    component.title = 'Awesome title';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render title', () => {
    const titleEl: HTMLElement = de.query(By.css('.modal-title')).nativeElement;
    expect(titleEl.innerText).toBe('Awesome title');
  });

  it('should dismiss modal on close button click', () => {
    const closeButton = de.query(By.css('button[type="button"]'));
    const modalService = TestBed.get(NgbActiveModal);
    const dismissSpy = spyOn(modalService, 'dismiss');
    closeButton.triggerEventHandler('click', {});
    expect(dismissSpy).toHaveBeenCalledTimes(1);
  });
});
