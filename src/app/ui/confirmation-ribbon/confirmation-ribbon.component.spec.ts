import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { ConfirmationRibbonComponent } from './confirmation-ribbon.component';

describe('ConfirmationRibbonComponent', () => {
  let component: ConfirmationRibbonComponent;
  let fixture: ComponentFixture<ConfirmationRibbonComponent>;
  let de: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmationRibbonComponent ],
      imports: [ NoopAnimationsModule ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmationRibbonComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    component.active = true;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not display ribbon if active is set to false', () => {
    component.active = false;
    fixture.detectChanges();
    const ribbon = de.query(By.css('.kd-ribbon'));
    expect(ribbon).toBeNull();
  });

  it('should display ribbon if active is set to true', () => {
    const ribbon = de.query(By.css('.kd-ribbon'));
    expect(ribbon instanceof DebugElement).toBe(true);
  });

  it('should render message', () => {
    const expectedMessage = 'Message';
    component.message = expectedMessage;
    fixture.detectChanges();
    const messageEl: HTMLElement = de.query(By.css('.kd-ribbon-message__text')).nativeElement;
    expect(messageEl.innerText).toBe(expectedMessage);
  });

  it('should emit confirm event with payload of true on consent button click', () => {
    const consentButton = de.query(By.css('#kd-consent-button'));
    component.confirm.subscribe((payload: boolean) => {
      expect(payload).toBe(true);
    });
    consentButton.triggerEventHandler('click', {});
  });

  it('should emit confirm event with payload of false on rejection button click', () => {
    const rejectionButton = de.query(By.css('#kd-rejection-button'));
    component.confirm.subscribe((payload: boolean) => {
      expect(payload).toBe(false);
    });
    rejectionButton.triggerEventHandler('click', {});
  });
});
