import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ConfirmationRibbonComponent } from './confirmation-ribbon.component';

@NgModule({
  declarations: [ConfirmationRibbonComponent],
  imports: [
    CommonModule,
    BrowserAnimationsModule
  ],
  exports: [ConfirmationRibbonComponent]
})
export class ConfirmationRibbonModule { }
