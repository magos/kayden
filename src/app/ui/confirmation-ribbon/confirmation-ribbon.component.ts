import { Component, Input, Output, EventEmitter } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'kd-confirmation-ribbon',
  templateUrl: './confirmation-ribbon.component.html',
  styleUrls: ['./confirmation-ribbon.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({ transform: 'translateY(100%)' }),
        animate('500ms ease-in-out', style({ transform: 'translateY(0%)' }))
      ]),
      transition(':leave', [
        animate('500ms ease-in-out', style({ transform: 'translateY(100%)' }))
      ])
    ])
  ]
})
export class ConfirmationRibbonComponent {

  @Input() message: string;
  @Input() active: boolean;
  @Output() confirm: EventEmitter<boolean>;

  constructor() {
    this.confirm = new EventEmitter<boolean>();
  }
}
