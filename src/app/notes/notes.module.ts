import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NoteService } from './shared/note.service';
import { NotesRoutingModule } from './notes-routing.module';
import { NotesPageComponent } from './pages/notes-page/notes-page.component';
import { NoteListResolveService } from './shared/note-list-resolve.service';
import { StatisticsPanelModule } from '../ui/statistics-panel/statistics-panel.module';
import { NoteListComponent } from './components/note-list/note-list.component';
import { NoteItemComponent } from './components/note-item/note-item.component';
import { CheckboxModule } from '../ui/checkbox/checkbox.module';
import { ConfirmationRibbonModule } from '../ui/confirmation-ribbon/confirmation-ribbon.module';
import { ModalModule } from '../ui/modal/modal.module';
import { ModalNoteFormComponent } from './components/modal-note-form/modal-note-form.component';
import { NoteFormComponent } from './components/note-form/note-form.component';
import { SortModule } from '../ui/sort/sort.module';

@NgModule({
  declarations: [
    NotesPageComponent,
    NoteListComponent,
    NoteItemComponent,
    ModalNoteFormComponent,
    NoteFormComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    NotesRoutingModule,
    StatisticsPanelModule,
    CheckboxModule,
    ConfirmationRibbonModule,
    ModalModule,
    SortModule,
    NgbModule
  ],
  providers: [
    NoteService,
    NoteListResolveService,
    NgbActiveModal
  ],
  entryComponents: [
    ModalNoteFormComponent
  ]
})
export class NotesModule { }
