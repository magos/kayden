import { NoteStatus } from './note-status.enum';

export interface Note {
  id: number;
  title: string;
  content: string;
  status: NoteStatus;
  isDisabled: boolean;
  isSelected: boolean;
}
