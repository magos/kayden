import { TestBed } from '@angular/core/testing';

import { NoteListResolveService } from './note-list-resolve.service';
import { NoteServiceStub } from '../../../testing/note.service.stub';
import { NoteService } from './note.service';

describe('NoteListResolveService', () => {
  let resolver: NoteListResolveService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: NoteService, useClass: NoteServiceStub },
        NoteListResolveService
      ]
    });
    resolver = TestBed.get(NoteListResolveService);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  describe('resolve', () => {
    it('should get notes while resolving', () => {
      const noteService = TestBed.get(NoteService);
      spyOn(noteService, 'getNotes');
      resolver.resolve();
      expect(noteService.getNotes).toHaveBeenCalledTimes(1);
    });
  });
});
