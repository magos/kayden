import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Note } from './note';
import { environment } from '../../../environments/environment';

@Injectable()
export class NoteService {

  constructor(private http: HttpClient) { }

  getNotes(): Observable<Note[]> {
    return this.http.get<Note[]>(`${environment.baseApiUrl}/notes`)
      .pipe(catchError(this.formatErrors));
  }

  insertNote(note: Note): Observable<Note> {
    return this.http.post<Note>(`${environment.baseApiUrl}/notes`, note)
      .pipe(catchError(this.formatErrors));
  }

  removeNote(id: number): Observable<Note> {
    return this.http.delete<Note>(`${environment.baseApiUrl}/notes/${id}`)
      .pipe(catchError(this.formatErrors));
  }

  private formatErrors(error: any) {
    return throwError(error.error);
  }
}
