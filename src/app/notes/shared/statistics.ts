export interface Statistics {
  completed: number;
  uncompleted: number;
  total: number;
}
