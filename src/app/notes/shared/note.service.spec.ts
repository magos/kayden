import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { NoteService } from './note.service';
import { getNotes, getNote } from '../../../testing/utilities';
import { Note } from './note';
import { environment } from '../../../environments/environment';

describe('NoteService', () => {
  let injector: TestBed;
  let service: NoteService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ NoteService ]
    });
    injector = getTestBed();
    service = injector.get(NoteService);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getNotes', () => {
    it('should return an Observable of Notes', () => {
      const expectedNotes = getNotes();

      service.getNotes().subscribe((notes: Note[]) => {
        expect(notes).toEqual(expectedNotes);
      });

      const req = httpMock.expectOne(`${environment.baseApiUrl}/notes`);
      expect(req.request.method).toBe('GET');
      req.flush(expectedNotes);
    });
  });

  describe('insertNote', () => {
    it('should POST expected note', () => {
      const newNote = getNote();

      service.insertNote(newNote).subscribe((addedNote: Note) => {
        expect(addedNote).toEqual(newNote);
      });

      const req = httpMock.expectOne(`${environment.baseApiUrl}/notes`);
      expect(req.request.method).toBe('POST');
      req.flush(newNote);
    });
  });

  describe('removeNote', () => {
    it('should DELETE note with specified identifier', () => {
      const noteToRemove = getNote();

      service.removeNote(noteToRemove.id).subscribe((removedNote: Note) => {
        expect(removedNote.id).toBe(noteToRemove.id);
      });

      const req = httpMock.expectOne(`${environment.baseApiUrl}/notes/${noteToRemove.id}`);
      expect(req.request.method).toBe('DELETE');
      req.flush(noteToRemove);
    });
  });

});
