import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { Observable } from 'rxjs';

import { Note } from './note';
import { NoteService } from './note.service';

@Injectable()
export class NoteListResolveService implements Resolve<Note[]> {

  constructor(private noteService: NoteService) { }

  resolve(): Observable<Note[]> {
    return this.noteService.getNotes();
  }
}
