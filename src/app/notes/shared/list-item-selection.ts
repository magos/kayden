export interface ListItemSelection {
  id: number;
  isSelected: boolean;
}
