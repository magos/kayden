export enum NoteStatus {
  New,
  Uncompleted,
  Completed
}
