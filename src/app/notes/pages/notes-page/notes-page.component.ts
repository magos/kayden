import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable, of, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { NoteService } from '../../shared/note.service';
import { Note } from '../../shared/note';
import { NoteStatus } from '../../shared/note-status.enum';
import { Statistics } from '../../shared/statistics';
import { ModalNoteFormComponent } from '../../components/modal-note-form/modal-note-form.component';

@Component({
  selector: 'kd-notes-page',
  templateUrl: './notes-page.component.html',
  styleUrls: ['./notes-page.component.scss']
})
export class NotesPageComponent implements OnInit, OnDestroy {

  notes$: Observable<Note[]>;
  statistics$: Observable<Statistics>;
  subscription: Subscription;

  constructor(private noteService: NoteService,
    private route: ActivatedRoute,
    private modal: NgbModal) {
      this.subscription = new Subscription();
    }

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      this.notes$ = of(routeData['notes']);
      this.statistics$ = this.calculateStatistics(this.notes$);
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  openAddModal(): void {
    const addModalRef: NgbModalRef = this.modal.open(ModalNoteFormComponent);
    addModalRef.componentInstance.title = 'News';
    this.subscription.add(addModalRef.componentInstance.formCancelled.subscribe(() => addModalRef.dismiss()));
    this.subscription.add(addModalRef.componentInstance.formSubmitted.subscribe((newNote: Note) => {
      this.addNote(newNote);
      addModalRef.close();
    }));
  }

  removeNote(id: number) {
    this.noteService.removeNote(id).subscribe(() => {
      this.refreshData();
    });
  }

  addNote(note: Note): void {
    this.noteService.insertNote(note).subscribe(() => {
      this.refreshData();
    });
  }

  private refreshData() {
    this.notes$ = this.fetchNotes();
    this.statistics$ = this.calculateStatistics(this.notes$);
  }

  private fetchNotes(): Observable<Note[]> {
    return this.noteService.getNotes();
  }

  private calculateStatistics(notes$: Observable<Note[]>): Observable<Statistics> {
    return notes$.pipe(map((notes: Note[]) => ({
      completed: notes.filter((note: Note) => note.status === NoteStatus.Completed).length,
      uncompleted: notes.filter((note: Note) => note.status === NoteStatus.Uncompleted).length,
      total: notes.length
    })));
  }
}
