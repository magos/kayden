import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { of, Subscription } from 'rxjs';

import { NotesPageComponent } from './notes-page.component';
import { NoteService } from '../../shared/note.service';
import { getNote, getNotes } from '../../../../testing/utilities';
import { NoteServiceStub } from '../../../../testing/note.service.stub';
import { Note } from '../../shared/note';
import { Statistics } from '../../shared/statistics';
import { NoteStatus } from '../../shared/note-status.enum';

describe('NotesPageComponent', () => {
  let component: NotesPageComponent;
  let fixture: ComponentFixture<NotesPageComponent>;
  const incomingData = getNotes();

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ NotesPageComponent ],
      schemas: [ NO_ERRORS_SCHEMA ],
      providers: [
        { provide: NoteService, useClass: NoteServiceStub },
        { provide: ActivatedRoute, useValue: {
            data: of({ notes: incomingData })
          }
        }
      ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize subscription', () => {
    expect(component.subscription instanceof Subscription).toBe(true);
  });

  it('should assign incoming data to notes$', () => {
    component.notes$.subscribe((notes: Note[]) => {
      expect(notes).toEqual(incomingData);
    });
  });

  it('should calculate total notes', () => {
    component.statistics$.subscribe((statistics: Statistics) => {
      expect(statistics.total).toBe(incomingData.length);
    });
  });

  it('should calculate completed notes', () => {
    const completedNotes = incomingData.filter((note: Note) => note.status === NoteStatus.Completed);
    component.statistics$.subscribe((statistics: Statistics) => {
      expect(statistics.completed).toBe(completedNotes.length);
    });
  });

  it('should calculate uncompleted notes', () => {
    const uncompletedNotes = incomingData.filter((note: Note) => note.status === NoteStatus.Uncompleted);
    component.statistics$.subscribe((statistics: Statistics) => {
      expect(statistics.uncompleted).toBe(uncompletedNotes.length);
    });
  });

  describe('OnDestroy', () => {
    it('should dispose subscriptions', () => {
      const unsubscribeSpy = spyOn(component.subscription, 'unsubscribe');
      component.ngOnDestroy();
      expect(unsubscribeSpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('removeNote', () => {

    it('should call service', () => {
      const noteIdToRemove = 3;
      const noteService = TestBed.get(NoteService);
      const removeNoteSpy = spyOn(noteService, 'removeNote').and.returnValue(of({ id: noteIdToRemove } as Note));
      component.removeNote(noteIdToRemove);
      expect(removeNoteSpy).toHaveBeenCalledTimes(1);
    });

    it('should fetch fresh data', () => {
      const noteIdToRemove = 3;
      const noteService = TestBed.get(NoteService);
      const getNotesSpy = spyOn(noteService, 'getNotes').and.returnValue(of(getNotes()));
      component.removeNote(noteIdToRemove);
      expect(getNotesSpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('addNote', () => {
    it('should call service', () => {
      const noteToAdd: Note = getNote();
      const noteService = TestBed.get(NoteService);
      const addNoteSpy = spyOn(noteService, 'insertNote').and.returnValue(of(noteToAdd));
      component.addNote(noteToAdd);
      expect(addNoteSpy).toHaveBeenCalledTimes(1);
    });

    it('should fetch fresh data', () => {
      const noteToAdd: Note = getNote();
      const noteService = TestBed.get(NoteService);
      const getNotesSpy = spyOn(noteService, 'getNotes').and.returnValue(of(getNotes()));
      component.addNote(noteToAdd);
      expect(getNotesSpy).toHaveBeenCalledTimes(1);
    });
  });
});
