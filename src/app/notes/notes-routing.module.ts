import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotesPageComponent } from './pages/notes-page/notes-page.component';
import { NoteListResolveService } from './shared/note-list-resolve.service';

const routes: Routes = [
  {
    path: '',
    component: NotesPageComponent,
    resolve: { notes: NoteListResolveService }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotesRoutingModule { }
