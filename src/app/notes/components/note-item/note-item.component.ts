import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Note } from '../../shared/note';
import { NoteStatus } from '../../shared/note-status.enum';
import { ListItemSelection } from '../../shared/list-item-selection';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'tr[kd-note-item]',
  templateUrl: './note-item.component.html',
  styleUrls: ['./note-item.component.scss']
})
export class NoteItemComponent {

  @Input() note: Note;
  @Output() itemSelectionChanged: EventEmitter<ListItemSelection>;

  noteStatuses = new Map<number, string>([
    [NoteStatus.New, 'New'],
    [NoteStatus.Uncompleted, 'Not completed'],
    [NoteStatus.Completed, 'Completed']
  ]);

  constructor() {
    this.note = {} as Note;
    this.itemSelectionChanged = new EventEmitter<ListItemSelection>();
  }

  checkboxChanged(isChecked: boolean): void {
    this.note.isSelected = isChecked;
    this.itemSelectionChanged.emit({
      id: this.note.id,
      isSelected: isChecked
    });
  }
}
