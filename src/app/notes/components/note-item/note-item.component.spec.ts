import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA, DebugElement, EventEmitter } from '@angular/core';

import { NoteItemComponent } from './note-item.component';
import { ListItemSelection } from '../../shared/list-item-selection';
import { getNote } from '../../../../testing/utilities';
import { NoteStatus } from '../../shared/note-status.enum';

describe('NoteItemComponent', () => {
  let component: NoteItemComponent;
  let fixture: ComponentFixture<NoteItemComponent>;
  let de: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ NoteItemComponent ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoteItemComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize note property', () => {
    expect(component.note).not.toBeUndefined();
  });

  it('should initialize itemSelectionChanged', () => {
    expect(component.itemSelectionChanged instanceof EventEmitter).toBe(true);
  });

  it('should render properties of input note', () => {
    const expectedNote = getNote();
    component.note = expectedNote;
    fixture.detectChanges();
    expect(de.nativeElement.textContent).toContain(expectedNote.id);
    expect(de.nativeElement.textContent).toContain(expectedNote.title);
    expect(de.nativeElement.textContent).toContain(expectedNote.content);
  });

  describe('checkboxChanged', () => {

    beforeEach(() => {
      component.note = getNote();
    });

    it('should change isSelected property of note', () => {
      component.checkboxChanged(true);
      expect(component.note.isSelected).toBe(true);
    });

    it('should emit itemSelectionChanged event', () => {
      const expectedValue = false;
      component.itemSelectionChanged.subscribe((selection: ListItemSelection) => {
        expect(selection).toEqual({ id: 2, isSelected: expectedValue});
      });
      component.checkboxChanged(expectedValue);
    });
  });
});
