import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormGroup } from '@angular/forms';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

import { NoteFormComponent } from './note-form.component';

describe('NoteFormComponent', () => {
  let component: NoteFormComponent;
  let fixture: ComponentFixture<NoteFormComponent>;
  let de: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ NoteFormComponent ],
      imports: [ ReactiveFormsModule]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoteFormComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should assign noteForm on initialization', () => {
    fixture.detectChanges();
    expect(component.noteForm).toBeTruthy();
  });

  it('should emit formCancelled event on cancel button click', () => {
    fixture.detectChanges();
    const cancelButton = de.query(By.css('button[type="button"]'));
    component.formCancelled.subscribe((payload) => {
      expect(payload).toBeUndefined();
    });
    cancelButton.triggerEventHandler('click', {});
  });

  describe('initializeForm', () => {
    it('should return form group', () => {
      const noteForm = component.initializeForm();
      expect(noteForm instanceof FormGroup).toBe(true);
    });

    it('should return form containing two properties', () => {
      const noteForm = component.initializeForm();
      const formProperties: string[] = Object.keys(noteForm.value);
      expect(formProperties.length).toBe(2);
    });

    it('should return form which has property of title', () => {
      const noteForm = component.initializeForm();
      expect(Object.keys(noteForm.value)).toContain('title');
    });

    it('should return form which has property of content', () => {
      const noteForm = component.initializeForm();
      expect(Object.keys(noteForm.value)).toContain('content');
    });
  });

  describe('submit', () => {

    beforeEach(() => {
      fixture.detectChanges();
    });

    it('should emit form submitted event with proper note object', () => {
      const expectedValue = { title: 'example title', content: 'example content' };
      component.noteForm.setValue(expectedValue);
      component.formSubmitted.subscribe((formValue) => {
        expect(formValue).toEqual(expectedValue);
      });
      component.submit(component.noteForm);
    });
  });
});
