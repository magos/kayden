import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';

@Component({
  selector: 'kd-note-form',
  templateUrl: './note-form.component.html',
  styleUrls: ['./note-form.component.scss']
})
export class NoteFormComponent implements OnInit {

  @Output() formSubmitted: EventEmitter<any>;
  @Output() formCancelled: EventEmitter<any>;
  noteForm: FormGroup;

  get title(): AbstractControl {
    return this.noteForm.get('title');
  }

  get content(): AbstractControl {
    return this.noteForm.get('content');
  }

  constructor(private fb: FormBuilder) {
    this.formSubmitted = new EventEmitter<any>();
    this.formCancelled = new EventEmitter<any>();
  }

  ngOnInit() {
    this.noteForm = this.initializeForm();
  }

  initializeForm(): FormGroup {
    return this.fb.group({
      title: ['', Validators.required],
      content: ['', Validators.required]
    });
  }

  submit(noteForm: FormGroup) {
    this.formSubmitted.emit(noteForm.value);
  }
}
