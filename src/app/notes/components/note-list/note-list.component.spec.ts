import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA, EventEmitter } from '@angular/core';

import { NoteListComponent } from './note-list.component';
import { getNotes } from '../../../../testing/utilities';
import { ListItemSelection } from '../../shared/list-item-selection';
import { Note } from '../../shared/note';

describe('NoteListComponent', () => {
  let component: NoteListComponent;
  let fixture: ComponentFixture<NoteListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ NoteListComponent ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoteListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize notes with empty array', () => {
    expect(component.notes).toEqual([]);
  });

  it('should initialize confirmation message', () => {
    expect(component.confirmationMessage.length).toBeGreaterThan(0);
  });

  it('should initialize noteDeleted event', () => {
    expect(component.noteDeleted instanceof EventEmitter).toBe(true);
  });

  it('should get undefined selectedNote if notes is undefined', () => {
    component.notes = undefined;
    expect(component.selectedNote).toBeUndefined();
  });

  describe('responseConfirmation', () => {
    it('should emit noteDeleted event with selected note identifier if confirmed parameter is true', () => {
      const confirmed = true;
      component.notes = getNotes();
      component.noteDeleted.subscribe((noteId: number) => {
        expect(noteId).toBe(component.selectedNote.id);
      });
      component.responseConfirmation(confirmed);
    });

    it('should release selection if confirmed parameter is false', () => {
      const confirmed = false;
      component.responseConfirmation(confirmed);
      expect(component.notes.every((note: Note) => note.isSelected === false && note.isDisabled === false)).toBe(true);
    });
  });

  describe('setSelectionOption', () => {
    it('should prevent further selection if input selection parameter has property of isSelected set to true', () => {
      const selectionInput: ListItemSelection = { id: 3, isSelected: true };
      component.setSelectionOption(selectionInput);
      const remainingNotes = component.notes.filter((note: Note) => note.id !== selectionInput.id);
      expect(remainingNotes.every(note => note.isDisabled === true)).toBe(true);
    });

    it('should release selection if input selection parameter has property of isSelected set to false', () => {
      const selectionInput: ListItemSelection = { id: 3, isSelected: false };
      component.setSelectionOption(selectionInput);
      expect(component.notes.every((note: Note) => note.isSelected === false && note.isDisabled === false)).toBe(true);
    });
  });
});
