import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Note } from '../../shared/note';
import { ListItemSelection } from '../../shared/list-item-selection';
import { SortingStateService } from '../../../ui/sort';

@Component({
  selector: 'kd-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.scss'],
  providers: [ SortingStateService ]
})
export class NoteListComponent {

  @Output() noteDeleted: EventEmitter<number>;
  @Input() notes: Note[];
  confirmationMessage: string;

  get selectedNote(): Note {
    return this.notes && this.notes.find((note: Note) => note.isSelected === true);
  }

  constructor() {
    this.notes = [];
    this.confirmationMessage = 'Do you want to delete selected notes?';
    this.noteDeleted = new EventEmitter();
  }

  responseConfirmation(confirmed: boolean) {
    confirmed ? this.noteDeleted.emit(this.selectedNote.id) : this.releaseSelection(this.notes);
  }

  trackByFn(index: number, item: Note): number {
    return item.id;
  }

  setSelectionOption(selectionInfo: ListItemSelection) {
    selectionInfo.isSelected ? this.preventSelection(this.notes, selectionInfo.id) : this.releaseSelection(this.notes);
  }

  private preventSelection(notes: Note[], selectedNoteId: number) {
    notes.map((note: Note) => note.id !== selectedNoteId ? note.isDisabled = true : note.isDisabled = false);
  }

  private releaseSelection(notes: Note[]) {
    notes.map((note: Note) => {
      note.isSelected = false;
      note.isDisabled = false;
    });
  }
}
