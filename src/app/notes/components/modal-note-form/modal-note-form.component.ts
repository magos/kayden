import { Component, Output, EventEmitter } from '@angular/core';

import { Note } from '../../shared/note';
import { NoteStatus } from '../../shared/note-status.enum';

@Component({
  selector: 'kd-modal-note-form',
  templateUrl: './modal-note-form.component.html',
  styleUrls: ['./modal-note-form.component.scss']
})
export class ModalNoteFormComponent {

  @Output() formSubmitted: EventEmitter<Note>;
  @Output() formCancelled: EventEmitter<boolean>;
  title: string;

  constructor() {
    this.formSubmitted = new EventEmitter<Note>();
    this.formCancelled = new EventEmitter<boolean>();
  }

  submitForm(note: Note) {
    note.status = note.status || NoteStatus.New;
    this.formSubmitted.emit(note);
  }

  cancelForm() {
    this.formCancelled.emit();
  }
}
