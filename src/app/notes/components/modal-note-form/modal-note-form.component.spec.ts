import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';

import { ModalNoteFormComponent } from './modal-note-form.component';
import { Note } from '../../shared/note';
import { getNote } from '../../../../testing/utilities';
import { NoteStatus } from '../../shared/note-status.enum';

describe('ModalNoteFormComponent', () => {
  let component: ModalNoteFormComponent;
  let fixture: ComponentFixture<ModalNoteFormComponent>;
  let de: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalNoteFormComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalNoteFormComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render modal', () => {
    const modalComponent: DebugElement = de.query(By.css('kd-modal'));
    expect(modalComponent).toBeTruthy();
  });

  it('should render form inside modal', () => {
    const noteFormComponent: DebugElement = de.query(By.css('kd-modal kd-note-form'));
    expect(noteFormComponent).toBeTruthy();
  });

  describe('submitForm', () => {
    it('should emit formSubmitted event with expected payload', () => {
      const exampleNote: Note = getNote();
      component.formSubmitted.subscribe((submittedNote: Note) => {
        expect(submittedNote).toBe(exampleNote);
      });
      component.submitForm(exampleNote);
    });

    it('should set status of emitted note to New if undefined', () => {
      const exampleNote: Note = getNote();
      exampleNote.status = undefined;
      component.formSubmitted.subscribe((submittedNote: Note) => {
        expect(submittedNote.status).toBe(NoteStatus.New);
      });
      component.submitForm(exampleNote);
    });
  });

  describe('cancelForm', () => {
    it('should emit formCancelled event', () => {
      component.formCancelled.subscribe((payload) => {
        expect(payload).toBe(undefined);
      });
      component.cancelForm();
    });
  });
});
