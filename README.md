# Kayden - Angular demo application
author: Marcin Gościniak

The app is built using [angular-cli](https://github.com/angular/angular-cli) version 7.2.2. See its documentation for more details.

## Install dependencies
Before you start application install all its dependencies. Run `npm install` command. (NodeJS and node package manager required)

## Launch API
To mimic API start `npm run api`. It launches json-server locally using `db.json` file. It is served on `http://localhost:3000`.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `npm test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
